const controllers = require('../controllers');

const ROUTES = {
  getUser: {
    method: 'GET',
    path: '/users/:userId',
    handler: controllers.getUser,
    requiresAuth: true,
    roles: ['admin']
  }
};

module.exports = ROUTES;