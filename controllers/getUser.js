const userServices = require('../services/users');

const getUser = async (req, res) => {

  try {

    // do any checks on the request here
    if (!['1','2'].includes(req.params?.userId)) {

      res.status(403);
      res.send({
        message: 'You are not allowed to access this resource'
      });

      return;
    }

    // you can use multiple services here

    const user = await userServices.getUser(req.params);

    if (!user) {

      res.status(404);
      res.send({
        message: 'User not found'
      });

      return;
    }

    res.send(user);

  }
  catch(e) {

    // we need to log this error

    console.error(e);

    res.status(500);
    res.send({
      message: 'Unfortunately a technical error occurred'
    });
  }
};

module.exports = getUser;