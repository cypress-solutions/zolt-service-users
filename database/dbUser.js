const getUserById = (id) => {

  // connect to the db to get the user

  const user = id === '1'
    ? { name: 'Hein' }
    : id === '2'
      ? { name: 'Marco' }
      : { name: 'Unknown' };

  return user;
};

module.exports = {
  getUserById
};