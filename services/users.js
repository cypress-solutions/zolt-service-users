const dbUser = require('../database/dbUser');
const agifyClient = require('../clients/agifyClient');

const getUser = async (params) => {

  // add any business logic here
  // use any clients to do 3rd party integration
  // here you can do internal microservice to microservice communication
  // here you can use db functions

  const user = dbUser.getUserById(params.userId);

  if (!user) {
    return null;
  }

  const userEstimatedAge = await agifyClient.getUserAgeEstimateByName(user.name);

  return {
    ...user,
    userId: params.userId,
    estimatedAge: userEstimatedAge
  };
};

module.exports = {
  getUser
};