const axios = require('axios');

const getUserAgeEstimateByName = async (name) => {

  if (!name) {
    return 0;
  }

  const res = await axios.get(`https://api.agify.io/?name=${name}`);

  if (res && res.data) {
    return res.data.age;
  }

  return 0;
};

module.exports = {
  getUserAgeEstimateByName
};