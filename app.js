const express = require('express');
const jwt = require('jsonwebtoken');
const cookieParser = require('cookie-parser');
const ROUTES = require('./routes/routes');
const PORT = 3030;
const SECRET = 'mhjFf98Jj#59#kfng!23knfdke#)plfjNekA';  // this will need to be moved

const app = express();
app.use(express.json());
app.use(cookieParser());

for (const route of Object.values(ROUTES)) {

  if (!route || !route.method || !route.path || !route.handler) {
    continue;
  }

  switch (route.method.toUpperCase()) {
    case 'GET':
      app.get(route.path, (req, res) => {
        bindRouteHandler(req, res, route);
      });
      break;
    case 'POST':
      app.post(route.path, (req, res) => {
        bindRouteHandler(req, res, route);
      });
      break;
    case 'PUT':
      app.put(route.path, (req, res) => {
        bindRouteHandler(req, res, route);
      });
      break;
    case 'DELETE':
      app.delete(route.path, (req, res) => {
        bindRouteHandler(req, res, route);
      });
      break;
    default:
      break;
  }
}

const bindRouteHandler = (req, res, route) => {

  if (!route.requiresAuth) {

    route.handler(req, res);
    return;
  }

  let token = req.cookies['Authorization'];

  jwt.verify(token, SECRET, function(err, decoded) {

    if (err) {

      res.status(401);
      res.send({
        message: 'You are unauthenticated'
      });

      return;
    }

    if (route.roles && route.roles.length && !route.roles.includes(decoded.role)) {

      res.status(403);
      res.send({
        message: 'You do not have access to the requested resource'
      });

      return;
    }

    route.handler(req, res);
  });
};

// handle all unhandled errors
app.use(function (err, req, res, next) {

  // we need to log all errors here, still need to be implemented

  console.error(err);

  res.status(500);
  res.send({
    message: 'Unfortunately a technical error occurred'
  });
});

app.listen(PORT, () => {
  console.log(`App listening at http://localhost:${PORT}`);
});